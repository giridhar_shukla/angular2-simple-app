# README #

### Build a simple Biodata App using Angular2.O and Typescript before its final release- ###

In this article, we are going to develop a super simple biodata app using Angular 2.O and Typescript.  Since Angular2 is still in Beta, so there might be some changes after its final release. I have build the Angular2.O quickstart app according to the guidelines in the angular.io. 

### About Angular 2 ###

Angular 2.O is a new google's framework that helps in developing Single page JavaScript application in a modular, maintainable and testable way. It is rewritten from Angular 1 following the best practices for future. This framework is written in TypeScript. TypeScript is just a superset of JavaScript. Any valid js code is a valid typescript. You need not to learn new language. TypeScript provides many features of ECMA Script 6 that are supported by many browsers like classes, interfaces, access modifiers (public, private), IntelliSense and compile time checking. And of course, Angular 2 has a vast community support and is backed by Google.

Unlike Angular 1, it has 4 key components:

* Components- Encapsulates the template, data and behaviour of the view.

* Directives- To modify DOM elements and/or extend their behaviour.

* Routers- To navigate in the application.

* Services- Encapsulates the data calls to backend, business logic and anything not related to view.


In Angular2 application, there must be one root component that will be the parent of all the child components. It is just like the tree of components where all the components are indivisual and reusable. Component is technically a directive but the difference is only that component has its template and directive hasn't.

Below is a sample component-

```
#!javascript


export class AppComponent { 

}
```

Yes, it is just a class that encapsulates some code. We exported the class so that other components can import it and use it accordingly. Similarly, Service is also a simple exported TypeScript class.

### PREREQUISITES ###
* NodeJs version >=4
* NPM version >=2

Follow the following steps to run this app:

1. npm install -g typescript

2. npm install -g typings

3. git clone https://giridhar_shukla@bitbucket.org/giridhar_shukla/angular2-simple-app.git

4. cd angular2-simple-app

5. npm install

6. npm start

7. see your app in localhost:3000


Note- If your changes don't reflect to the browser, just restart the server.

### Structure of the application ###

```
#!javascript

angular2-simple-app
|--- app
      |--- app.component.ts
      |--- biodata-form.component.ts
      |--- biodata-form.component.html
      |--- biodata.ts
      |--- boot.ts
|--- node_modules ...
|--- typings ...
|--- index.html
|--- package.json
|--- styles.css
|--- forms.css
|--- tsconfig.json
|--- typings.json
```


After compilation, two files- js and map file are created for each ts files in the application.  Browsers use js file to understand the ts file and respective map file helps to identify the errors in typescript files.

### Description of application files- ###

**tsconfig.json**

It is the configuration file for TypeScript compiler. It determines how to transpile the typescript files to javascript files. Such as target property of compilerOptions indiactes that it will transpile in ES5.

Angular website is the good resource if you want to know more about all the configuration files. 


```
#!javascript

{
  "compilerOptions": {
    "target": "es5",
    "module": "system",
    "moduleResolution": "node",
    "sourceMap": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "removeComments": false,
    "noImplicitAny": false
  },
  "exclude": [
    "node_modules",
    "typings/main",
    "typings/main.d.ts"
  ]
}

```


**typings.json**

It is another configuration file for TypeScript. When using external JavaScript libraries in TypeScript, we need to import TypeScript Definition file. This file gives static file checking and intellisense for that JavaScript file.  

```
#!javascript

{
  "ambientDependencies": {
    "es6-shim": "github:DefinitelyTyped/DefinitelyTyped/es6-shim/es6-shim.d.ts#6697d6f7dadbf5773cb40ecda35a76027e0783b2"
  }
}
```


**package.json**

It is a standard node package manager configuration file that includes name, version, dependencies for the application. When we npm start our app, our typescript compiler is run in watch mode and run the server.


** index.html **

It is the web page that hosts the application where all the required libraries, css files are loaded. In this file, System Js is configured and the main TypeScript file that starts our application is run. In our case, it is boot.ts. The selector <my-app> of our root component is also present in this file. When our root component is loaded successfully, all the templates replaces this selector and renders in the web page automatically.


```
#!javascript

<html>
  <head>
      <title>Angular 2 Simple App</title>
      <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
      <link rel="stylesheet" href="styles.css">
      <link rel="stylesheet" href="forms.css">
    <!-- 1. Load libraries -->
    <!-- IE required polyfills, in this exact order -->
    <script src="node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="node_modules/systemjs/dist/system-polyfills.js"></script>
    <script src="node_modules/angular2/bundles/angular2-polyfills.js"></script>
    <script src="node_modules/systemjs/dist/system.src.js"></script>
    <script src="node_modules/rxjs/bundles/Rx.js"></script>
    <script src="node_modules/angular2/bundles/angular2.dev.js"></script>
    <!-- 2. Configure SystemJS -->
    <script>
      System.config({
        packages: {        
          app: {
            format: 'register',
            defaultExtension: 'js'
          }
        }
      });
      System.import('app/boot')
            .then(null, console.error.bind(console));
    </script>
  </head>
  <!-- 3. Display the application -->
  <body>
    <my-app>Loading...</my-app>
  </body>
</html>
```


Note- All our application related source code is in App folder.

**App.component.ts**

This is the root component file of our application. Here first we imported component decorator from core library of Angular2. Then that decorator function is called and passed a required keys. Selector is string that is put in the index.html file to indicate the root component rendered view. Template or TemplateUrl is used to set the view for the component. Any Angular 2 app has one root component and many child components. In our case, BiodataFormComponent is the child component of the root component. To use sub component in parent component, we need to import the class and put that in the directives array as in the beolw code.
In template of this component, we used Template strings of ES6. The html code can be written in formatted way using Template String.


```
#!javascript

import {Component} from 'angular2/core';
import {BiodataFormComponent} from './biodata-form.component';
@Component({
    selector: 'my-app',
    template: `
        <h1>My First Angular 2 App</h1>
        <biodata-form></biodata-form>
        `,
    directives: [BiodataFormComponent]
})
export class AppComponent { }

```

**boot.ts**

This file is the main starting point of angular2 app. We import the two things we need to launch the application: 
1. Angular's browser bootstrap function,
2. the application root component, AppComponent.
Then we call bootstrap with AppComponent.


```
#!javascript

import {bootstrap}    from 'angular2/platform/browser'
import {AppComponent} from './app.component'
bootstrap(AppComponent);
```

**biodata.ts**

As users enter form data, we capture their changes and update an instance of a model. We can't layout the form until we know what the model looks like.
A model can be as simple as a "property bag" that holds facts about a thing of application importance. In our case, we have put name , phone and qualification as params of our constructor.


```
#!javascript

export class Biodata {
    constructor(
        public id: number,
        public name: string,
        public phone: string,
        public qualification?: string
    ) {  }
}

```

**biodata-form.component.ts**

It is similar to AppComponent instead of some new things. We imported the Biodata model class to use in this component. NgForm is used to dealt with the form related stuffs such as validation etc. In our component class, we have an qualifyList array that displays data in form's select box. We created an instance of the Biodata class that will reference the form fields. The function saveBioData is used to save the form data and create new instance of the Biodata class.


```
#!javascript

import {Component} from 'angular2/core';
import {NgForm}    from 'angular2/common';
import { Biodata }    from './biodata';
@Component({
    selector: 'biodata-form',
    templateUrl: 'app/biodata-form.component.html'
})
export class BiodataFormComponent {
    qualifyList = ['Be/B.Tech', 'M.Tech', 'BA/BSc', 'MA/MSc','MCA'];
    allFormData =[];
    model = new Biodata();
    saveBiodata() {
        this.allFormData.push(this.model);
        this.model = new Biodata();
     }
}

```


**biodata-form.component.html**

This is the template for our biodata-form.component. Here we have a form, a div for live loading of data as we change the form fields and a div where all the biodata are displayed. Initially there is no data saved. We have just created an array in the component for saving the data. So as you refresh the page, all data will get removed.

PROPERTY BINDING and EVENT BINDING-

We used [(ngModel)] that makes binding our form to the model. Angular 2.O use [] syntax for property binding and () syntax for event binding. We can see the [hidden] and [value] directive  that indicates the propety binding and (click) directive that indicates the click event on submission of the form.
 
In a Property Binding, a value flows from the model to a target property on screen. We identify that target property by surrounding its name in brackets, []. This is a one-way data binding from the model to the view.

In an Event Binding, we flow the value from the target property on screen to the model. We identify that target property by surrounding its name in parentheses, (). This is a one-way data binding in the opposite direction from the view to the model.

No wonder Angular chose to combine the punctuation as [()] to signify a two-way data binding and a flow of data in both directions.

By setting ngControl, we create a directive that can tell if the user touched the control, if the value changed, or if the value became invalid.
This directive doesn't just track state; it updates the control with special Angular CSS classes from the set we listed above. We can leverage those class names to change the appearance of the control and make messages appear or disappear.


Also We defined a template local variable, #biodataForm, and initialized it with the value, "ngForm". The variable biodataForm is now a reference to the ngForm directive that governs the form as a whole. Using this, we disabled the submit button if form is invalid.


Here *ngIf="" is used to show/hide the view on the basis of some condition. If condition is true, it displays the inner content, else it removes it.
Similarly, *ngFor="#data of allFormData" is used to loop through the array that is set in the component. This syntax is Angular2 specific. Each time, #data will be different and will traverse the allFormData array.


{{}} is the interpolation directive where the expression value is displayed in the view.



```
#!javascript

<div class="container">
    <!--{{checkCurrentModel}}-->
    <h3>Biodata Form:</h3>
    <form #biodataForm="ngForm">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" required
                   [(ngModel)]="model.name"
                    ngControl="name" #name="ngForm">
            <div [hidden]="name.valid || name.pristine" class="alert alert-danger">Name is required</div>
        </div>
        <div class="form-group">
            <label for="alterEgo">Contact Number</label>
            <input type="text" class="form-control" [(ngModel)]="model.phone" maxlength="10">
        </div>
        <div class="form-group">
            <label for="power">Qualification</label>
            <select class="form-control" [(ngModel)]="model.qualification">
                <option *ngFor="#p of qualifyList" [value]="p">{{p}}</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default" (click)="saveBiodata()" [disabled]="!biodataForm.form.valid">Submit</button>
    </form>
    <h3>Live view of the form data:</h3>
    <div class="row">
        <div class="col-xs-3">Name</div>
        <div class="col-xs-9  pull-left">{{ model.name }}</div>
    </div>
    <div class="row">
        <div class="col-xs-3">Alter Ego</div>
        <div class="col-xs-9 pull-left">{{ model.phone }}</div>
    </div>
    <div class="row">
        <div class="col-xs-3">Power</div>
        <div class="col-xs-9 pull-left">{{ model.qualification }}</div>
    </div>
<br/>
    <!-- all data listing -->
    <h3>Biodata list:</h3>
    <div *ngIf="allFormData && allFormData === ''" *ngFor="#data of allFormData">
        <div class="row">
            <div class="col-xs-3">Name</div>
            <div class="col-xs-9  pull-left">{{ data.name }}</div>
        </div>
        <div class="row">
            <div class="col-xs-3">Alter Ego</div>
            <div class="col-xs-9 pull-left">{{ data.phone }}</div>
        </div>
        <div class="row">
            <div class="col-xs-3">Power</div>
            <div class="col-xs-9 pull-left">{{ data.qualification }}</div>
        </div>
        <br/>
    </div>
</div>
```