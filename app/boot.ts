//this is the file where we bootstrap our root component.
//We import the two things we need to launch the application:
//Angular's browser bootstrap function and the application root component, AppComponent.
//Then we call bootstrap with AppComponent.

import {bootstrap}    from 'angular2/platform/browser';
import {AppComponent} from './app.component';

bootstrap(AppComponent);
