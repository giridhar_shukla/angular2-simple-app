//As users enter form data, we capture their changes and update an instance of a model. We can't layout the form until we know what the model looks like.
//A model can be as simple as a "property bag" that holds facts about a thing of application importance.
//this class has 3 fields that are recognisable by their names.....hehehe
export class Biodata {
    constructor(
        public id: number,
        public name: string,
        public phone: string,
        public qualification?: string
    ) {  }
}