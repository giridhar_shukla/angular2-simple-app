import {Component} from 'angular2/core';
import {NgForm}    from 'angular2/common';
import { Biodata }    from './biodata';

@Component({
    selector: 'biodata-form',
    templateUrl: 'app/biodata-form.component.html'
})
export class BiodataFormComponent {
    qualifyList = ['Be/B.Tech', 'M.Tech', 'BA/BSc', 'MA/MSc','MCA'];// this  is list of qualification select box

    allFormData =[];//we put saved biodata in this array to display all in the bottom of the page

    model = new Biodata();//create an instance of the class

    //uncomment this line and this {{checkCurrentModel}} in its template to see the model instance getting updated on form change
    //get checkCurrentModel() { return JSON.stringify(this.model); }

    //this function saves the form data on clicking save button and inserts in the allFormData array
    //and it creates new instance
    saveBiodata() {
        this.allFormData.push(this.model);
        this.model = new Biodata();
     }
}