//import component decorator
import {Component} from 'angular2/core';
//import biodata component to use it in this component's template but before that,
// put this component in the directives array of AppComponent's decorator.
import {BiodataFormComponent} from './biodata-form.component';

//call the component function and provide an object;
//selector is the string that is used to indicate the component
//i.e. if this selector matches in the view, it creates an instance of the respective component;
//template is the view that is loaded for this selector;
//directives is an array where we put all the directives and components that is needed in this component; 
@Component({
    selector: 'my-app',
    template: `
        <h1>My First Angular 2 App</h1>
        <biodata-form></biodata-form>
        `,
    directives: [BiodataFormComponent]
})

//define root component class
export class AppComponent { }